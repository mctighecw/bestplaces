import sys
from src.app import create_app

if __name__ == '__main__':
    app = create_app()

    port_number = 7000

    print(f'--- Starting web server on port {port_number}')
    app.run(port=port_number, host='0.0.0.0')
