import os
import logging

from flask_cors import CORS
from src.bp_app import bp_app

def create_app():
    app = bp_app

    FLASK_ENV = os.getenv('FLASK_ENV')

    if FLASK_ENV == 'production':
        log_level = logging.INFO
    else:
        CORS(app)
        log_level = logging.DEBUG

    logging.basicConfig(format='%(asctime)s %(levelname)s [%(module)s %(lineno)d] %(message)s', level=log_level)

    return app
