from flask import Flask
import os
import json
import time
import wikipedia
import urllib.request

default_headers = {"Access-Control-Allow-Credentials": "true"}

bp_app = Flask(__name__)


@bp_app.route('/api/wiki/summary/<string:article>', methods=['GET'])
def get_summary(article):
    return wikipedia.summary(article), 200, default_headers


@bp_app.route('/api/wiki/page/url/<string:page>', methods=['GET'])
def get_page_url(page):
    wiki_page = wikipedia.page(page)
    return wiki_page.url, 200, default_headers


@bp_app.route('/api/wiki/page/content/<string:page>', methods=['GET'])
def get_page_content(page):
    wiki_page = wikipedia.page(page)
    return wiki_page.content, 200, default_headers


@bp_app.route('/api/geodata/<string:code>', methods=['GET'])
def get_geobytes_data(code):
    try:
        # Free API key from geobytes.com
        API_KEY = "7c756203dbb38590a66e01a5a3e1ad96";

        seconds = time.time()
        seconds_str = str(seconds).replace(".", "")
        geobytes_url = f'https://secure.geobytes.com/GetCityDetails?key={API_KEY}&fqcn={code}&_={seconds_str}'

        with urllib.request.urlopen(geobytes_url) as response:
           data = json.load(response)

        return data, 200, default_headers

    except:
        return 'Error getting geobytes data', 400, default_headers


@bp_app.route('/api/weather/<string:city>', methods=['GET'])
def get_weather_data(city):
    try:
        WEATHER_KEY = os.getenv('WEATHER_KEY')
        weather_url = f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={WEATHER_KEY}&units=metric'

        with urllib.request.urlopen(weather_url) as response:
           data = json.load(response)

        return data, 200, default_headers

    except:
        return 'Error getting weather data', 400, default_headers
