# README

This is a React app that provides info on some of the best places to live in the world, based on the Mercer Quality of Living Surveys in [2017](https://www.mercer.com/newsroom/2017-quality-of-living-survey.html) and [2018](https://mobilityexchange.mercer.com/Insights/quality-of-living-rankings).

The app uses three APIs to fetch live data from the Internet.
Once the Wikipedia and GeoBytes data have been received, it is put into the Redux store and is subsequently retrieved from there.
The weather data is fetched initially, saved in the Redux store, and then updated every five minutes.

## App Information

App Name: bestplaces

Created: Spring 2018

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/bestplaces)

Production: [Link](http://bestplaces.mctighecw.site)

## Notes

### Frontend
* React
* Redux
* React Router
* Webpack
* ZURB Foundation
* Sass

### Backend
* Flask

### Deployment
* Docker
* NGINX

### APIs
* [Wikipedia API for Python](https://pypi.python.org/pypi/wikipedia)
* [GeoBytes](http://geobytes.com)
* [OpenWeather](https://openweathermap.org/api)

### Photos
All of the city photos were found via Google (only images that were labeled for reuse and not under copyright).

Last updated: 2025-02-05
