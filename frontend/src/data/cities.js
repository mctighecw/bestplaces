const cities = [
  {
    place: 1,
    name: 'Vienna',
    country: 'Austria',
    image: 'vienna.jpg',
    geoBytesCode: 'ATWIVIEN'
  },
  {
    place: 2,
    name: 'Zurich',
    country: 'Switzerland',
    image: 'zurich.jpg',
    geoBytesCode: 'CHZHZURI'
  },
  {
    place: 3,
    name: 'Auckland',
    country: 'New Zealand',
    image: 'auckland.jpg',
    geoBytesCode: 'NZAUAUCK'
  },
  {
    place: 4,
    name: 'Munich',
    country: 'Germany',
    image: 'munich.jpg',
    geoBytesCode: 'DEBYMUNI'
  },
  {
    place: 5,
    name: 'Vancouver',
    country: 'Canada',
    image: 'vancouver.jpg',
    geoBytesCode: 'CABCVANC'
  },
  {
    place: 6,
    name: 'Dusseldorf',
    country: 'Germany',
    image: 'dusseldorf.jpg',
    geoBytesCode: 'DENWDUSS'
  },
  {
    place: 7,
    name: 'Frankfurt',
    country: 'Germany',
    image: 'frankfurt.jpg',
    geoBytesCode: 'DEHEFRAN'
  },
  {
    place: 8,
    name: 'Geneva',
    country: 'Switzerland',
    image: 'geneva.jpg',
    geoBytesCode: 'CHGEGENE'
  },
  {
    place: 9,
    name: 'Copenhagen',
    country: 'Denmark',
    image: 'copenhagen.jpg',
    geoBytesCode: 'DKSKCOPE'
  },
  {
    place: 10,
    name: 'Sydney',
    country: 'Australia',
    image: 'sydney.jpg',
    geoBytesCode: 'AUNSSYDN'
  }
];

export default cities;
