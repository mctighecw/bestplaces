const request = new XMLHttpRequest();

export const getRequest = (url,type) => {
  return new Promise((resolve, reject) => {
    request.open('GET', url, true);
    request.responseType = '';
    request.setRequestHeader('Content-type', type);
    request.send();

    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        if (request.status === 200) {
          const data = request.responseText;
          resolve(data);
        } else {
          reject();
        }
      }
    }
  });
}
