export const formatNumber = (n) => {
  return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export const convertCelsiusToFahrenheit = (c) => {
  return c * (9 / 5) + 32;
}

export const importImages = (r) => {
  let images = {};
  r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
  return images;
}

export const getTimeDifference = (present, previous) => {
  const diff = present.getTime() - previous.getTime();
  const mins = diff / 60000;
  let update;

  if (mins > 5.0) {
    console.log(`${mins.toFixed(1)} min. since last weather update`);
    update = true;
  } else {
    update = false;
  }
  return update;
}

export const validateJSON = (str) => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}
