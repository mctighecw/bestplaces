const location = process.env.NODE_ENV === 'development' ? 'http://localhost:7000/api' : '/api';

export const geoBytesUrl = `${location}/geodata`;
export const weatherUrl = `${location}/weather`;
export const wikiUrl = `${location}/wiki/summary`;
