import React from 'react';
import { Switch, Route } from 'react-router-dom';

import LandingPage from './components/LandingPage';
import Main from './components/Main';

import './styles/styles.scss';

const App = () => (
  <Switch>
    <Route exact path='/' component={LandingPage} />
    <Route path='/cities' component={Main} />
  </Switch>
);

export default App;
