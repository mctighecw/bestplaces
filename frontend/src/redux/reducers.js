import { combineReducers } from 'redux';

import { SAVE_WIKI_TEXT, SAVE_GEOBYTES_DATA, UPDATE_WEATHER_INFO } from './actionTypes';

let initialState = {};

for (let i = 0; i < 10; i++) {
  initialState[i] = {
    wikiText: '',
    geoBytesData: [],
    weather: {
      timestamp: '',
      info: []
    }
  }
};

const cityReducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_WIKI_TEXT:
      return {
        ...state,
        [action.index]: {
          ...state[action.index],
          wikiText: action.text
        }
      }
    case SAVE_GEOBYTES_DATA:
      return {
        ...state,
        [action.index]: {
          ...state[action.index],
          geoBytesData: action.data
        }
      }
    case UPDATE_WEATHER_INFO:
      return {
        ...state,
        [action.index]: {
          ...state[action.index],
          weather: {
            timestamp: action.timestamp,
            info: action.info
          }
        }
      }
    default:
      return state
  }
};

const rootReducer = combineReducers({
  cities: cityReducer
});

export default rootReducer;
