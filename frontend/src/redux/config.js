import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import logger from 'redux-logger';
import rootReducer from './reducers';

const persistConfig = {
  key: 'root',
  storage
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const middlewares = compose(applyMiddleware(logger));

export const store = process.env.NODE_ENV === 'development' ? createStore(persistedReducer, middlewares) : createStore(persistedReducer);
export const persistor = persistStore(store);
