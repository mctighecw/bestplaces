import { SAVE_WIKI_TEXT, SAVE_GEOBYTES_DATA, UPDATE_WEATHER_INFO } from './actionTypes';

export const saveWikiText = (index, text) => ({
  type: SAVE_WIKI_TEXT,
  index,
  text
});

export const saveGeoBytesData = (index, data) => ({
  type: SAVE_GEOBYTES_DATA,
  index,
  data
});

export const updateWeatherInfo = (index, timestamp, info) => ({
  type: UPDATE_WEATHER_INFO,
  index,
  timestamp,
  info
});
