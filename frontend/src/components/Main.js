import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Column, Button, ButtonGroup } from 'react-foundation';

import { saveWikiText, saveGeoBytesData, updateWeatherInfo } from '../redux/actions';
import { getRequest } from '../utils/requests';
import { geoBytesUrl, wikiUrl, weatherUrl, weatherSuffix } from '../utils/network';
import { validateJSON, getTimeDifference } from '../utils/functions';

import TopMenu from './TopMenu';
import CityInfo from './city/CityInfo';
import Footer from './Footer';
import cities from '../data/cities';

const styles = {
  page: {
    fontFamily: 'Roboto',
    backgroundColor: '#f1f1f1',
    width: '100%',
    maxHeight: '100vh',
    overflow: 'scroll'
  },
  content: {
    padding: '15px 15px 50px 15px'
  },
  hr: {
    height: 1,
    background: 'gray',
    background: '-webkit-gradient(linear, 0 0, 100% 0, from(#f1f1f1), to(#f1f1f1), color-stop(50%, #666))',
    maxWidth: '80vw',
    margin: '10px auto'
  },
  buttons: {
    textAlign: 'center',
    marginTop: 20
  },
  buttonLeft: {
    display: 'inline-block',
    marginRight: 10
  },
  buttonRight: {
    display: 'inline-block',
    marginLeft: 10
  },
  buttonDropdown: {
    marginBottom: 0
  },
};

class Main extends Component {
  constructor(props) {
  super(props);
    this.state = {
      cityIndex: 0
    };
  }

  getGeoByteData = () => {
    return new Promise((resolve, reject) => {
      const selectedCity = cities[this.state.cityIndex];
      const url = `${geoBytesUrl}/${selectedCity.geoBytesCode}`;
      const index = selectedCity.place - 1;

      getRequest(url, 'text/json')
        .then(res => {
          const validJSON = validateJSON(res);

          if (validJSON) {
            const data = JSON.parse(res);
            this.props.onSaveGeoBytesData(index, data);
            resolve();
            console.log('GeoByte data received');
          } else {
            resolve();
            console.log('An error has occurred getting GeoByte data');
          }
        })
        .catch(err => {
          resolve();
          console.log(err);
        });
    });
  }

  getWikiData = () => {
    return new Promise((resolve, reject) => {
      const selectedCity = cities[this.state.cityIndex];
      const url = `${wikiUrl}/${selectedCity.name}`;
      const index = selectedCity.place - 1;

      getRequest(url, 'text/json')
        .then(res => {
          this.props.onSaveWikiText(index, res);
          resolve();
          console.log('Wiki data received');
        })
        .catch(err => {
          console.log(err);
          resolve();
        });
    });
  }

  getWeather(city) {
    const selectedCity = cities[this.state.cityIndex];
    const url = `${weatherUrl}/${city}`;
    const index = selectedCity.place - 1;
    const timestamp = (new Date()).toISOString();

    getRequest(url, 'text/json')
      .then(res => {
        const data = JSON.parse(res);
        this.props.onUpdateWeatherInfo(index, timestamp, data);
        console.log('Weather data received');
      })
      .catch(err => {
        console.log('An error has occurred getting weather data');
      });
  }

  checkForWeatherData = () => {
    return new Promise((resolve, reject) => {
      const selectedCity = cities[this.state.cityIndex];
      const cityData = this.props.cities[this.state.cityIndex];

      if (cityData.weather && cityData.weather.info.length === 0) {
        const result = 'Fetching initial weather data';
        this.getWeather(selectedCity.name);
        resolve(result);
      } else {
        const previous = new Date(cityData.weather.timestamp);
        const present = new Date();
        const shouldUpdate = getTimeDifference(present, previous);

        if (shouldUpdate) {
          this.getWeather(selectedCity.name);
          resolve();
          console.log('Fetching new weather data')
        } else {
          resolve();
          console.log('No need to get weather data')
        }
      }
    });
  }

  getData = async () => {
    const cityData = this.props.cities[this.state.cityIndex];

    if (cityData.wikiText === '') await this.getWikiData();
    if (cityData.geoBytesData.length === 0) await this.getGeoByteData();
    await this.checkForWeatherData();
  }

  handleClickForward = () => {
    const { cityIndex } = this.state;
    const citiesLength = cities.length;

    if (cityIndex +1 < citiesLength) {
      this.setState({ cityIndex: cityIndex +1 }, () => {
        this.getData();
      });
    } else {
      this.setState({ cityIndex: 0 }, () => {
        this.getData();
      });
    }
  }

  handleClickBackward = () => {
    const { cityIndex } = this.state;
    const citiesLength = cities.length;

    if (cityIndex > 0) {
      this.setState({ cityIndex: cityIndex -1 }, () => {
        this.getData();
      });
    } else {
      this.setState({ cityIndex: citiesLength -1 }, () => {
        this.getData();
      });
    }
  }

  handleClickGoToIndex = (index) => {
    if (this.state.cityIndex !== index) {
      this.setState({ cityIndex: index });
    }
  }

  componentDidMount() {
    this.getData();
  }

  render() {
    const selectedCity = cities[this.state.cityIndex];

    return (
      <div style={styles.page}>
        <TopMenu
          cities={cities}
          cityIndex={this.state.cityIndex}
          handleClickGoToIndex={this.handleClickGoToIndex}
        />

        <div style={styles.content}>
          <CityInfo
            selectedCity={selectedCity}
          />

          <div style={styles.hr} />

          <div style={styles.buttons}>
            <Button size="large" onClick={this.handleClickBackward} style={styles.buttonLeft}>{"<<  Back"}</Button>
            <Button size="large" onClick={this.handleClickForward} style={styles.buttonRight}>{"Next  >>"}</Button>
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}

Main.propTypes = {
  cities: PropTypes.object.isRequired,
  onSaveWikiText: PropTypes.func.isRequired,
  onSaveGeoBytesData: PropTypes.func.isRequired,
  onUpdateWeatherInfo: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  cities: state.cities
});

const mapDispatchToProps = dispatch => ({
  onSaveWikiText: (index,text) => {dispatch(saveWikiText(index,text))},
  onSaveGeoBytesData: (index,data) => {dispatch(saveGeoBytesData(index,data))},
  onUpdateWeatherInfo: (index,timestamp,info) => {dispatch(updateWeatherInfo(index,timestamp,info))}
});

const MainConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);

export default MainConnect;
