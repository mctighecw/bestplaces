import React, { Component } from 'react';
import { TopBar, TopBarItem, TopBarTitle, Menu, MenuItem, Button, Dropdown } from 'react-foundation-components';
import ListDropdown from './ListDropdown';

const backgroundColor = '#fff';

const styles = {
  menu: {
    backgroundColor: backgroundColor,
    borderBottom: '1px solid #2199e8'
  },
  background: {
    backgroundColor: backgroundColor
  },
  heading: {
    backgroundColor: backgroundColor,
    fontSize: 22
  },
  button: {
    width: 149,
    margin: '0 30px 0 0'
  }
};

class TopMenuBar extends Component {
  constructor(props) {
  super(props);
    this.state = {
      menuOpen: false
    };
  }

  render() {
    return (
      <TopBar style={styles.menu}>
        <TopBarTitle style={styles.background}>
          <Menu style={styles.background}>
            <MenuItem text style={styles.heading}>Best in the World</MenuItem>
          </Menu>
        </TopBarTitle>
        <TopBarItem position="right">
          <Button
            size="small"
            dropdown
            style={styles.button}
            onClick={() => { this.setState({ menuOpen: !this.state.menuOpen }); }}
          >
            Complete List
          </Button>

          <ListDropdown
            menuOpen={this.state.menuOpen}
            cities={this.props.cities}
            cityIndex={this.props.cityIndex}
            handleClickGoToIndex={this.props.handleClickGoToIndex}
          />


        </TopBarItem>
      </TopBar>
    );
  }
}

export default TopMenuBar;
