import React from 'react';

const styles = {
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    height: 50,
    backgroundColor: '#2e3840'
  },
  text: {
    color: '#fff',
    margin: '15px 0 0 15px'
  }
};

const Footer = () => (
  <footer style={styles.footer}>
    <p style={styles.text}>&copy; 2018, Christian McTighe. Coded by Hand.</p>
  </footer>
);

export default Footer;
