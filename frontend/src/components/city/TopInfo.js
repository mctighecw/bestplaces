import React from 'react';
import { Row, Column } from 'react-foundation';
import { importImages } from '../../utils/functions';

const images = importImages(require.context('../../assets/cities', false, /\.jpe?g$/));

const styles = {
  leftDiv: {
    width: '100%',
    float: 'right',
    marginTop: '14%'
  },
  rightDiv: {
    textAlign: 'right'
  },
  place: {
    fontSize: 46,
    fontWeight: 500,
    textAlign: 'center',
    margin: 0
  },
  city: {
    fontSize: 42,
    fontWeight: 500,
    textAlign: 'center',
    margin: 0
  },
  image: {
    display: 'inline-block',
    borderRadius: 5,
    width: '70%',
    minWidth: 300,
    height: 'auto'
  },
  noPadding: {
    padding: 0
  }
};

const TopInfo = ({ selectedCity }) => (
  <Row className="display">
    <Column small={6} style={styles.noPadding}>
      <div style={styles.leftDiv}>
        <h1 style={styles.place}>#{selectedCity.place}</h1>
        <h1 style={styles.city}>{selectedCity.name}, {selectedCity.country}</h1>
      </div>
    </Column>
    <Column small={6} style={styles.noPadding}>
      <div style={styles.rightDiv}>
        <img
          src={images[selectedCity.image]}
          alt="city pic"
          style={styles.image} />
      </div>
    </Column>
  </Row>
);

export default TopInfo;
