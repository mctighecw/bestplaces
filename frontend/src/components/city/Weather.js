import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { convertCelsiusToFahrenheit } from '../../utils/functions';
import 'open-weather-icons/scss/open-weather-icons.scss';

const styles = {
  container: {
    minHeight: 85
  },
  outerDiv: {
    display: 'inline-block'
  },
  weatherText: {
    display: 'inline-block',
    verticalAlign: 'middle',
    marginRight: 8
  },
  weatherIcon: {
    display: 'inline-block',
    verticalAlign: 'middle'
  },
  text: {
    lineHeight: 1.4,
    letterSpacing: '0.015em',
    margin: '5px 0'
  }
};

const Weather = ({ selectedCity, cities }) => {
  const { info } = cities[selectedCity.place - 1].weather;

  return (
    <div style={styles.container}>
      {info.length !== 0 ?
        <div>
          <div style={styles.outerDiv}>
            <div style={styles.weatherText}>{info.weather[0].main}</div>
            <i className={"owi owi-" + info.weather[0].icon} style={styles.weatherIcon}></i>
          </div>
          <div>
            <p style={styles.text}>
              Current Temp.: {Math.round(info.main.temp)}°C / {Math.round(convertCelsiusToFahrenheit(info.main.temp))}°F<br />
              Min Temp.: {info.main.temp_min}°C / {Math.round(convertCelsiusToFahrenheit(info.main.temp_min))}°F<br />
              Max Temp.: {info.main.temp_max}°C / {Math.round(convertCelsiusToFahrenheit(info.main.temp_max))}°F
            </p>
          </div>
        </div>
        :
        <p><i>None available.</i></p>
      }
    </div>
  );
}

Weather.propTypes = {
  selectedCity: PropTypes.object.isRequired,
  cities: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  cities: state.cities
});

const WeatherConnect = connect(
  mapStateToProps,
  null
)(Weather);

export default WeatherConnect;
