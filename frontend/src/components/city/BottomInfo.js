import React from 'react';
import { Row, Column } from 'react-foundation';
import { formatNumber } from '../../utils/functions';
import Weather from './Weather';

const styles = {
  container: {
    padding: 2,
  },
  box: {
    backgroundColor: '#e5e5e5',
    padding: 10,
  },
  header: {
    fontWeight: 500,
    margin: '0 0 8px 0'
  },
  scrollBox: {
    height: 150,
    overflowY: 'scroll'
  },
  text: {
    lineHeight: 1.4,
    letterSpacing: '0.015em',
    margin: '5px 0'
  },
  link: {
    marginLeft: 10,
    textDecoration: 'none'
  },
  flexRow: {
    display: 'flex'
  },
  flexColumn: {
    flex: '50%'
  },
  noPadding: {
    padding: 0
  }
};

const BottomInfo = ({ selectedCity, wikiText, geoBytesData }) => (
  <Row className="display">
    <Column small={12} style={styles.noPadding}>
      <div style={styles.container}>
        <div style={styles.box}>
          <h2 style={styles.header}>Description</h2>
          <div style={styles.scrollBox} className='wikiBox'>
            <p style={styles.text}>
              {wikiText}
              {wikiText.length > 100 ?
                <a href={`http://www.wikipedia.com/wiki/${selectedCity.name}`} target='_blank' style={styles.link}>
                  Read more
                </a>
                : null
              }
            </p>
          </div>
        </div>
      </div>
    </Column>

    <Column small={8} style={styles.noPadding}>
      <div style={styles.container}>
        <div style={styles.box}>
          <h2 style={styles.header}>Facts</h2>
            <div style={styles.flexRow}>
              <div style={styles.flexColumn}>
                <p style={styles.text}>
                  Area: {geoBytesData[0]}<br />
                  Nationality: {geoBytesData[1]}<br />
                  Country Population: {formatNumber(geoBytesData[2])}<br />
                  Currency: {geoBytesData[3]} ({geoBytesData[4]})
                </p>
              </div>

              <div style={styles.flexColumn}>
                <p style={styles.text}>
                  Internet suffix: {geoBytesData[5]}<br />
                  Latitude: {geoBytesData[6]}<br />
                  Longitude: {geoBytesData[7]}<br />
                  Timezone: GMT {geoBytesData[8]}
                </p>
              </div>
            </div>
        </div>
      </div>
    </Column>

    <Column small={4} style={styles.noPadding}>
      <div style={styles.container}>
        <div style={styles.box}>
          <h2 style={styles.header}>Current Weather</h2>
          <div style={styles.text}>
            <Weather
              selectedCity={selectedCity}
            />
          </div>
        </div>
      </div>
    </Column>
  </Row>
);

export default BottomInfo;
