import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import InfoGrid from './InfoGrid';

const CityInfo = ({ selectedCity, cities }) => {
  const { wikiText, geoBytesData } = cities[selectedCity.place - 1];
  const wikiTexts = wikiText ? wikiText : "Loading...";
  const geoBytesDatas = geoBytesData && geoBytesData.geobytescapital ?
    [ geoBytesData.geobytesmapreference,
      geoBytesData.geobytesnationalitysingular,
      geoBytesData.geobytespopulation,
      geoBytesData.geobytescurrency,
      geoBytesData.geobytescurrencycode,
      geoBytesData.geobytesinternet,
      geoBytesData.geobyteslatitude,
      geoBytesData.geobyteslongitude,
      geoBytesData.geobytestimezone
    ]
    : "---------";

  return (
    <InfoGrid
      selectedCity={selectedCity}
      wikiText={wikiTexts}
      geoBytesData={geoBytesDatas}
    />
  );
}

CityInfo.propTypes = {
  selectedCity: PropTypes.object.isRequired,
  cities: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  cities: state.cities
});

const CityInfoConnect = connect(
  mapStateToProps,
  null
)(CityInfo);

export default CityInfoConnect;
