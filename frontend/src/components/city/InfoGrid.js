import React from 'react';
import TopInfo from './TopInfo';
import BottomInfo from './BottomInfo';

const styles = {
  page: {
    margin: '20px auto 0 auto'
  }
};

const InfoGrid = ({ selectedCity, wikiText, geoBytesData }) => (
  <div style={styles.page}>
    <TopInfo
      selectedCity={selectedCity}
    />

    <BottomInfo
      selectedCity={selectedCity}
      wikiText={wikiText}
      geoBytesData={geoBytesData}
    />
  </div>
);

export default InfoGrid;
