import React, { Component } from 'react';
import { Dropdown, Menu, MenuItem } from 'react-foundation-components';

const styles = {
  dropdown: {
    width: 115,
    backgroundColor: '#e6e6e6'
  },
  menu: {
    paddingLeft: 0
  }
};

const ListDropdown = ({ menuOpen, cities, cityIndex, handleClickGoToIndex }) => (
  <div>
    {menuOpen ?
      <Dropdown style={styles.dropdown}>
        <Menu style={styles.menu}>
          {cities.map((city, index) => {
              return (
                <div key={index}>
                  <MenuItem
                    onClick={() => { handleClickGoToIndex(index); }}
                    style={{ cursor: 'pointer', fontWeight: cityIndex === index ? 600 : 300 }}
                  >
                    <div>{city.place}. {city.name}</div>
                  </MenuItem>
                  {cities.length === index + 1 ? null : <br /> }
                </div>
              )
            })
          }
        </Menu>
      </Dropdown>
      : null
    }
  </div>
);

export default ListDropdown;
