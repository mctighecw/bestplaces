import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'react-foundation';

let styles = {
  page: {
    fontFamily: 'Roboto',
    backgroundColor: '#f1f1f1',
    width: '100vw',
    height: '100vh'
  },
  section: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    width: 650,
    backgroundColor: '#fff',
    border: '1px solid #ccc',
    borderRadius: 5,
    padding: 20
  },
  header: {
    textAlign: 'center',
    margin: 0
  },
  text: {
    lineHeight: 1.4,
    letterSpacing: '0.015em'
  },
  centerText: {
    textAlign: 'center'
  },
  buttonStyle: {
    display: 'inline-block', margin: 0
  }
};

class LandingPage extends Component {
  constructor(props) {
  super(props);
    this.state = {
      showFirstMessage: true
    };
  }

  render() {
    const { showFirstMessage } = this.state;

    const firstMessage = (
      <div>
        <div style={styles.text}>
          <p>Every year, <a href="https://www.mercer.com/" target="_blank">Mercer</a> publishes a <a href="https://mobilityexchange.mercer.com/Insights/quality-of-living-rankings" target="_blank">ranking of cities</a> with the best quality of life.</p>
          <p>European cities, particularly those in the German-speaking countries, tend to dominate the list year after year.</p>
          <p>Here's a look at the top cities from the both the 2017 and 2018 rankings.</p>
        </div>

        <div style={styles.centerText}>
          <Button
            size="large"
            style={styles.buttonStyle}
            onClick={() => { this.setState({ showFirstMessage: false }) }}
          >
            Continue
          </Button>
        </div>
      </div>
    );

    const secondMessage = (
      <div>
        <div>
          <div style={styles.text}>
            <p><b>Note:</b> This is a <i>dynamic</i> app, which fetches data from three sources on the Internet.</p>
            <p>The first time that each city loads, there might be a pause while the data is fetched.</p>
            <p>Subsequently, the city data will be cached and the weather info will be updated every five minutes.</p>
          </div>

          <div style={styles.centerText}>
            <Link to='/cities'>
              <Button size="large" style={styles.buttonStyle}>See the cities</Button>
            </Link>
          </div>
        </div>
      </div>
    );

    return (
      <div style={styles.page}>
        <div style={styles.section}>
          <h1 style={styles.header}>Best Places</h1>
          {showFirstMessage ? firstMessage : secondMessage}
        </div>
      </div>
    )
  }
}

export default LandingPage;
