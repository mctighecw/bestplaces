#!/bin/sh
echo "Building new Docker images and starting containers for bestplaces..."

docker rmi "bestplaces_frontend:latest"
docker rmi "bestplaces_backend:latest"

docker-compose up --build
